var express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  mongoose = require("mongoose"),
  passport = require("passport"),
  connectFlash = require("connect-flash"),
  methodOverride = require("method-override"),
  LocalStrategy = require("passport-local"),
  User = require("./models/user");
// seedDB      = require("./seeds");

// requiring routes
var indexRoute = require("./routes/index"),
  campgroundsRoute = require("./routes/campgrounds"),
  commentsRoute = require("./routes/comments");

mongoose.connect(
  "mongodb://admin:admin1@ds039301.mlab.com:39301/123l33t123",
  { useNewUrlParser: true }
);
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));
app.use(connectFlash());
// seedDB(); // seeds database

// PASSPORT CONFIGURATION
app.use(
  require("express-session")({
    secret: "MY LITTLE SECRET TO HASH PAssWorDs aye",
    resave: false,
    saveUninitialized: false
  })
);

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next) {
  res.locals.currentUser = req.user;
  res.locals.flashError = req.flash("error");
  res.locals.flashSuccess = req.flash("success");
  next();
});
app.use(methodOverride("_method"));

app.use("/campgrounds", campgroundsRoute);
app.use("/campgrounds/:id/comments", commentsRoute);
app.use(indexRoute);

const PORT = process.env.PORT || 8080;

app.listen(PORT);
