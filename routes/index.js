var express  = require("express"),
    router   = express.Router(),
    User     = require("../models/user"),
    passport = require("passport");
    
// root route
router.get("/", (req, res) => {
    res.render("landing");
});

// register form route
router.get("/register", (req, res) => {
    res.render("register");
});

// register logic
router.post("/register", (req, res) => {
    var newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, (err, user) => {
        if (err) {
            req.flash("error", err.message);
            return res.redirect("/register");
        }
        
        passport.authenticate("local")(req, res, () => {
            req.flash("success", "Welcome to YelpCamp " + user.username);
            res.redirect("/campgrounds");
        });
    });
});

// login form route
router.get("/login", (req, res) => {
   res.render("login");
});

// login logic
router.post("/login", passport.authenticate("local", 
    {
        successRedirect: "/campgrounds",
        failureRedirect: "/login"
    }), (req, res) => {});

router.get("/logout", (req, res) => {
    req.logout();
    req.flash("success", "Logged you out!")
    res.redirect("/campgrounds");
});

router.get("*", (req, res) => {
   res.redirect("/"); 
});

module.exports = router;
