var express = require("express"),
    router = express.Router(),
    Campground = require("../models/campground"),
    middleware = require("../middleware/index.js");

// index route
router.get("/", (req, res) => {
    Campground.find(function(err, campgrounds) {
       if (!err) {
            res.render("campgrounds/index", {campgrounds: campgrounds});
       } else {
           console.log(err);
       }
    });
});

// new campgroud route
router.get("/new", middleware.isLoggedIn, (req, res) => {
    res.render("campgrounds/new");
});

// create campgroud route
router.post("/", middleware.isLoggedIn, (req, res) => {
    var name  = req.body.name ,
        price = req.body.price,
        image = req.body.image ,
        desc  = req.body.description,
        author = {
            id: req.user._id,
            username: req.user.username
        };
    var newCampground = {name: name, price: price, image: image, description: desc, author: author};
    Campground.create(newCampground, function(err) {
        if (err) {
            req.flash("error", "Could not create campground");
            res.redirect("/campgrounds");
        }
    });  
    req.flash("success", "Created new campground");
    res.redirect("/campgrounds");
});

// show campgroud route
router.get("/:id", (req, res) => {
    Campground.findById(req.params.id).populate("comments").exec((err, foundCampground) => {
        if (!err && foundCampground) {
            res.render("campgrounds/show", {campground: foundCampground});
        } else {
            req.flash("error", "Campground not found");
            res.redirect("/campgrounds");
        }
    });
});

// Edit route
router.get("/:id/edit", middleware.checkCampgroundOwnership, (req, res) => {
    Campground.findById(req.params.id, (err, foundCampground) => {
            if (err) {
                req.flash("error", "Campground not found");
                res.redirect("/campgrounds");
            } else {
                res.render("campgrounds/edit", {campground: foundCampground});
            }
       }
    );
});

// Update route
router.put("/:id", middleware.checkCampgroundOwnership, (req, res) => {
   Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err) => {
       if (!err) {
           req.flash("success", "Updated campground");
           res.redirect("/campgrounds/" + req.params.id);
       } else {
           req.flash("error", "Campground not found");
           res.redirect("/campgrounds");
       }
   });
});

// Delete route
router.delete("/:id", middleware.checkCampgroundOwnership, (req, res) => {
    Campground.findByIdAndRemove(req.params.id, (err) => {
       if (err) {
           req.flash("error", "Campground not found");
           res.redirect("/campgrounds");
       } else {
           req.flash("success", "Successfully deleted campground");
           res.redirect("/campgrounds");
       }
    });
});

module.exports = router;
