var express    = require("express"),
    router     = express.Router({mergeParams: true}),
    Campground = require("../models/campground"),
    Comment    = require("../models/comment"),
    middleware = require("../middleware/index.js");

// New comment
router.get("/new", middleware.isLoggedIn, (req, res) => {
    Campground.findById(req.params.id, (err, campground) => {
       if (!err) {
            res.render("comments/new", {campground: campground});
       } else {
           res.redirect("/campgrounds/" + campground._id + "/comments");
       }
           
    });
});

// Create comment
router.post("/", middleware.isLoggedIn, (req, res) => {
    Campground.findById(req.params.id, (err, campground) => {
        if (err || !campground) {
            req.flash("error", "Could not find campground");
            res.redirect("/campgrounds");
        } else {
            Comment.create(req.body.comment, (err, comment) => {
                if (!err && comment) {
                    comment.author.id = req.user.id;
                    comment.author.username = req.user.username;
                    comment.save();
                    campground.comments.push(comment);
                    campground.save();
                    req.flash("success", "Successfully added comment")
                    res.redirect("/campgrounds/" + campground._id);
                } else {
                    req.flash("error", "Could not create comment");
                    res.redirect("/campgrounds/" + req.params.id);
                }
            });
        }
    });
});

// Edit comment
router.get("/:comment_id/edit", middleware.checkCommentOwnership, function(req, res) {
   Campground.findById(req.params.id, (err, foundCampground) => {
       if (err || !foundCampground) {
           req.flash("error", "Could not find campground");
           res.redirect("/campgrounds");
       } else {
            Comment.findById(req.params.comment_id, (err, foundComment) => {
               if (err || !foundComment) {
                   req.flash("error", "Could not find campground");
                   res.redirect("back");
               } else {
                   res.render("comments/edit", {campground_id: req.params.id, comment: foundComment});
               }
            });
       }
   });
   
});

// Update comment
router.put("/:comment_id", middleware.checkCommentOwnership, (req, res) => {
   Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, (err, comment) => {
       if (err || !comment) {
           req.flash("error", "Something went wrong");
           res.redirect("back");
       } else {
           req.flash("success", "Successfully updated comment")
           res.redirect("/campgrounds/" + req.params.id);
       }
   }) ;
});

// Delete comment
router.delete("/:comment_id", middleware.checkCommentOwnership, (req, res) => {
   Comment.findByIdAndRemove(req.params.comment_id, (err) => {
       if (err) {
           req.flash("error", "Something went wrong");
           res.redirect("/campgrounds");
       } else {
           req.flash("success", "Successfully deleted comment");
           res.redirect("/campgrounds/" + req.params.id);
       }
   });
});

module.exports = router;
